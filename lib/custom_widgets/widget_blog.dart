import 'package:flutter/material.dart';
import '../blog_details_page.dart';

Widget widgetBlog({
  required String imagePath,
  required String title,
  required String subtitle,
  required String content,
  required BuildContext context,
}) =>
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlogDetailsPage(
                            title: title,
                            imagePath: imagePath,
                            subtitle: subtitle,
                            content: content,
                          )));
            },
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              imagePath,
              height: 200.0,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.black.withOpacity(0.05),
          ),
        ),
        SizedBox(height: 10.0),
        InkWell(
          onTap: () {
            print(title);
          },
          borderRadius: BorderRadius.circular(20.0),
          child: Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          width: 50.0,
          child: Divider(
            thickness: 5,
            color: Colors.black,
          ),
        ),
        SizedBox(
          height: 20.0,
        ),
      ],
    );
