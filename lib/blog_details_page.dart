import 'package:flutter/material.dart';

class BlogDetailsPage extends StatelessWidget {
  final String title;
  final String imagePath;
  final String subtitle;
  final String content;

  BlogDetailsPage({
    required this.title,
    required this.imagePath,
    required this.subtitle,
    required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          title,
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              Container(
                color: Colors.black.withOpacity(0.05),
                child: Image.asset(
                  imagePath,
                  height: 200.0,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.0),
                    Text(
                      subtitle,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Text(content, style: TextStyle(color: Colors.blueGrey[900]))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
