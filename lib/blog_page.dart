import 'package:flutter/material.dart';
import 'custom_widgets/widget_blog.dart';

class BlogPage extends StatelessWidget {
  List<Widget> widgetBlogList = [];

  @override
  Widget build(BuildContext context) {
    initWidgetBlogList(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "The Blog",
          style: TextStyle(
            color: Colors.black,
            letterSpacing: 2.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...widgetBlogList,
              TextButton(
                onPressed: () {
                  print("Terms and Conditions");
                },
                child: Text(
                  "Terms and Conditions",
                  style: TextStyle(color: Colors.blueGrey[900]),
                ),
                style: TextButton.styleFrom(
                  minimumSize: Size(double.infinity, 45),
                ),
              ),
              TextButton(
                onPressed: () {
                  print("Sign Out");
                  Navigator.pop(context);
                },
                child: Text(
                  "Sign Out",
                  style: TextStyle(color: Colors.red),
                ),
                style: TextButton.styleFrom(
                  minimumSize: Size(
                    double.infinity,
                    45,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  initWidgetBlogList(BuildContext context) {
    widgetBlogList = [
      widgetBlog(
        imagePath: "images/rich.png",
        title: "The 10 Tips to retired at 40 years old",
        subtitle: "The best tips to retired early",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque commodo venenatis mi, a commodo metus semper non. Etiam non nibh dictum, tincidunt velit sit amet, molestie velit. Donec sodales velit sed feugiat dignissim. Sed consectetur, ligula at facilisis rutrum, lacus tortor commodo lectus, vitae lacinia neque massa quis nunc. Duis quis pharetra elit, a porttitor tellus. Vestibulum et vulputate felis. Praesent et malesuada elit. Maecenas at ligula quam. Nulla vulputate mi ac dolor lacinia, quis pretium orci molestie. Sed et rutrum nisi, imperdiet sodales tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
        context: context,
      ),
      widgetBlog(
        imagePath: "images/house.png",
        title: "How to purchase a property in 5 steps",
        subtitle: "You must acknowledge this before shopping",
        content:
            "Integer viverra porta nulla id fringilla. Duis ac feugiat arcu, pulvinar lobortis risus. Mauris suscipit, arcu sit amet facilisis pulvinar, lacus risus accumsan magna, in sollicitudin ex neque eu quam. Praesent congue diam efficitur dolor varius fringilla. In hac habitasse platea dictumst. Praesent nec mauris tellus. Curabitur sit amet diam nec dolor consectetur lobortis. Morbi scelerisque nunc sit amet arcu tristique accumsan. Nulla eu sapien consequat, sagittis dolor eu, porta eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer sit amet libero ullamcorper, sodales erat in, tincidunt nulla. In sed libero id velit molestie maximus. Phasellus pellentesque tincidunt tellus eu tempor.",
        context: context,
      ),
      widgetBlog(
        imagePath: "images/apps.png",
        title: "7 Apps to increase productivity",
        subtitle: "These apps will bost your daily tasks",
        content:
            "Aliquam lacus massa, feugiat ac ultrices sed, laoreet sed enim. Praesent leo urna, viverra vitae ipsum et, sodales varius libero. Donec gravida metus et ex scelerisque, eu consectetur quam suscipit. Morbi tellus metus, porttitor condimentum risus eu, faucibus consequat leo. Aenean dapibus leo iaculis tortor placerat, vel consectetur tortor varius. Aliquam eget ornare diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vehicula dolor tempus, ornare lectus vitae, molestie nunc. Nulla facilisi. Proin ultrices nisi sit amet pharetra facilisis. Etiam ut nisl at lorem rhoncus aliquet vitae at ipsum. Nulla commodo, augue sit amet gravida lacinia, mi tortor pharetra sapien, quis eleifend dolor mauris in ipsum.",
        context: context,
      ),
    ];
  }
}
